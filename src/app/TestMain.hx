package app;
import app.mutator.ColorMutator;
import app.viewport.ViewPort;
import openfl.events.TimerEvent;
import openfl.Lib;
import openfl.utils.Timer;

/**
 * ...
 * @author Creative Magic
 */
class TestMain
{
	var updateTimer:Timer;
	var port:ViewPort;
	var mutator:ColorMutator;

	public function new() 
	{
		setTimer();
		setPort();
		
		startTest();
	}
	
	function setPort() 
	{
		port = new ViewPort( Lib.current.stage.stageWidth, Lib.current.stage.stageHeight, Lib.current.stage);
		port.mutator = new ColorMutator();
		port.spriteAmount = 20;
		port.init();
	}
	
	function startTest() 
	{
		updateTimer.start();
	}
	
	function setTimer() 
	{
		updateTimer = new Timer( 16 );
		updateTimer.addEventListener(TimerEvent.TIMER, tick);
	}
	
	// EVENT HANDLERS
	
	private function tick(e:TimerEvent):Void 
	{
		port.update();
	}
	
}