package app.mutator;
import app.colors.Tint;
import app.mutator.ColorMutator.MutationType;
import app.viewport.ViewPort;
import openfl.Assets;
import openfl.display.BitmapData;
import openfl.display.Sprite;
import openfl.display.BlendMode;
import openfl.geom.Matrix;

/**
 * ...
 * @author Creative Magic
 */
enum MutationType
{
	Blend;
	Alpha;
	Fill;
	Tint;
}
 
class ColorMutator
{
	public var target:ViewPort;
	
	private var currentMutationType:MutationType;
	private var currentSprite:Sprite;
	
	private var bitmaps:Array<BitmapData> =
	[
		Assets.getBitmapData("img/drogo.jpg", false),
		Assets.getBitmapData("img/cat.jpg", false),
		Assets.getBitmapData("img/rug.jpg", false),
		Assets.getBitmapData("img/joshua.jpg", false)
	];
	
	private var blendModeTypes:Array<BlendMode> =
	[
		BlendMode.ADD,
		BlendMode.ALPHA,
		BlendMode.DARKEN,
		BlendMode.DIFFERENCE,
		BlendMode.ERASE,
		BlendMode.HARDLIGHT,
		BlendMode.INVERT,
		BlendMode.LIGHTEN,
		BlendMode.MULTIPLY,
		BlendMode.OVERLAY,
		BlendMode.SCREEN,
		BlendMode.SUBTRACT,
		BlendMode.LAYER,
	];

	public function new() 
	{
		
	}
	
	public function setSpriteStartMutation(targetSprite:Sprite):Void
	{
		changeAlpha(targetSprite);
		changeFill(targetSprite);
		changeTint(targetSprite);
		changeBlendType(targetSprite);
	}
	
	public function updateSprite(targetSprite:Sprite) 
	{		
		currentSprite = targetSprite;
		var allEnums = Type.allEnums(MutationType);
		currentMutationType = allEnums[Std.random(allEnums.length)];
		
		changeSprite(currentMutationType);
	}
	
	// PRIVATE METHODS
	
	function getRandomBitmapData():BitmapData
	{
		if (Math.random() > .5)
		return bitmaps[Std.random(bitmaps.length)];
		else
		{
			var isTransparent:Bool = (Math.random() > .5) ? true : false;
			var startBD = new BitmapData(500, 500, isTransparent, (isTransparent == true ) ? 0x00000000 : 200000000);
			
			for (i in 0...bitmaps.length)
			{
				var targetBD = bitmaps[Std.random(bitmaps.length)];
				var m:Matrix = new Matrix();
				m.translate( -500 + Std.random(1000), -500 + Std.random(1000));
				startBD.draw( targetBD, m, null, blendModeTypes[Std.random(blendModeTypes.length)], null, (Math.random() > .5) ? true : false );
			}
			
			return startBD;
		}
	}
	
	function changeSprite(currentMutationType:MutationType) 
	{
		switch ( currentMutationType )
		{
			case MutationType.Alpha:
				changeAlpha(currentSprite);
			case MutationType.Blend:
				changeBlendType(currentSprite);
			case MutationType.Fill:
				changeFill(currentSprite);
			case MutationType.Tint:
				changeFill(currentSprite);
		}
	}
	
	function changeBlendType(object:Sprite):Void
	{
		var selectedBlendMode = blendModeTypes[Std.random(blendModeTypes.length)];
		object.blendMode = selectedBlendMode;
	}
	
	function changeAlpha(object:Sprite):Void
	{
		object.alpha = Math.random();
	}
	
	function changeFill(object:Sprite):Void
	{
		object.graphics.clear();
		if (Math.random() > .5)
		object.graphics.beginFill( Std.random( 200000000 ));
		else
		{
			var m:Matrix = new Matrix();
			m.translate( Std.random( 500 ), Std.random( 500 ));
			object.graphics.beginBitmapFill( getRandomBitmapData(), m);
		}
		
		if (Math.random() > .5)
		object.graphics.drawCircle( 0, 0, Std.random( 50 ) + 90 );
		else
		object.graphics.drawRect( 0, 0, Std.random( 50 ) + 90, Std.random( 50 ) + 90);
		
		object.graphics.endFill();
	}
	
	function changeTint(object:Sprite):Void
	{
		var targetColor = Std.random(200000000);
		app.colors.Tint.tintObject(object, targetColor, Math.random());
		
	}
	
}