package app.colors;
import openfl.display.DisplayObject;
import openfl.geom.ColorTransform;

/**
 * ...
 * @author Creative Magic
 */
class Tint
{

	public static function tintObject( targetObject:DisplayObject, color:Int, alpha:Float = 1 )
	{
		targetObject.transform.colorTransform = tintClip(color, alpha);
	}
	
	public static function getTintedColorTransform(color:Int, alpha:Float = 1):ColorTransform
	{
		return tintClip( color, alpha);
	}
	
	private static function tintClip(tintColor:Int, tintAlpha:Float):ColorTransform
	{
		var ctMul:Float = (1 - tintAlpha);
		
		var ctRedOff:Float = Math.round(tintAlpha * extractRed(tintColor));
		var ctGreenOff:Float = Math.round(tintAlpha * extractGreen(tintColor));
		var ctBlueOff:Float = Math.round(tintAlpha * extractBlue(tintColor));
		
		return new ColorTransform(ctMul, ctMul, ctMul, 1, ctRedOff, ctGreenOff, ctBlueOff, 0);
	}
		
	private static function extractRed(c:Int):Int
	{
		return (( c >> 16 ) & 0xFF);
	}
	 
	private static function extractGreen(c:Int):Int
	{
		return ( (c >> 8) & 0xFF );
	}
	 
	private static function extractBlue(c:Int):Int
	{
		return ( c & 0xFF );
	}
	
}