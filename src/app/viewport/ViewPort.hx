package app.viewport;
import app.colors.Tint;
import app.mutator.ColorMutator;
import openfl.display.DisplayObjectContainer;
import openfl.display.Sprite;

/**
 * ...
 * @author Creative Magic
 */
class ViewPort
{
	public var spriteAmount:Int;
	public var mutator:ColorMutator;
	
	private var portWidth:Int;
	private var portHeight:Int;
	private var portContainer:DisplayObjectContainer;
	
	private var sprites:Array<Sprite> = [];
	private var spriteTransforms:Array<SpriteTransformParams> = [];
	
	private var accessNum:Int = -1;

	public function new(w:Int, h:Int, targetDisplayContainer:DisplayObjectContainer) 
	{
		portWidth = w;
		portHeight = h;
		portContainer = targetDisplayContainer;
	}
	
	public function init() 
	{
		for ( i in 0...spriteAmount)
		{
			var s:Sprite = new Sprite();
			s.x = Math.random() * portWidth;
			s.y = Math.random() * portHeight;
			portContainer.addChild(s);
			
			sprites.push(s);
			
			var st:SpriteTransformParams = new SpriteTransformParams();
			spriteTransforms.push(st);
			
			mutator.setSpriteStartMutation(s);
		}
	}
	
	public function update() 
	{
		for ( i in 0...sprites.length)
		{
			var sprite = sprites[i];
			var spriteTransform = spriteTransforms[i];
			
			sprite.x += spriteTransform.speedX;
			sprite.y += spriteTransform.speedX;
			sprite.rotation += spriteTransform.speedRot;
			
			if (sprite.x >= portWidth)
			{
				spriteTransform.reset();
				sprite.x = 0;
				mutator.updateSprite( sprite );
			}
			
			if (sprite.y >= portHeight)
			{
				spriteTransform.reset();
				sprite.y = 0;
				mutator.updateSprite( sprite );
			}
		}
	}
	
	public function getSprite():Sprite
	{
		accessNum++;
		return sprites[accessNum % sprites.length];
	}
	
	public function getAllSprites() 
	{
		return sprites;
	}
	
}

private class SpriteTransformParams
{
	public var speedX:Float = Math.random() * 2;
	public var speedY:Float = Math.random() * 2;
	public var speedRot:Float = Math.random() * .5;
	
	public function new()
	{
		
	}
	
	public function reset():Void
	{
		speedX = Math.random() * 2;
		speedY = Math.random() * 2;
		speedRot = Math.random() * .5;
	}
	
}